function round(A) {
	return int(A + 0.5)
}
BEGIN {
	d = 6. / 512
	eps = 1E-9
	print "#r/d  U  Vr  Uvr  T"
}
($2==0.672) {
	r = sqrt($3*$3 + $4*$4)
	rd = round(r*100) / 100 / d
	Vr = ($3*$8 + $4*$9) / (r + eps)
	
	print rd, $7, Vr, $7*Vr, $32
}
