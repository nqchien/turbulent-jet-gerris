#!/bin/env python

f = open('xs_PAP20_2j.pts', 'w')

# L = [0.5, 0.667, 0.833, 1, 1.167, 1.333, 1.5, 1.667, 1.833, 2, 2.167, 2.333]
L = [0.0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1.021, 1.125, 1.25, 1.5]

for x in L:
    for y in xrange(-30,31):
        f.write(str(x)+" "+str(y*0.01)+" 0\n")
    for z in xrange(-35,36):
        f.write(str(x)+" 0 "+str(z*0.01)+"\n")
f.close()
