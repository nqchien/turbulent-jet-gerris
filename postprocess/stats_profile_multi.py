""" Reads a time series file from gerris
and plot velocity and turbulent intensity profile
of one cross section denoted by xcr.
This value of xcr is input as a parameter from the command line.

Command line format:
python stats_profile.py output.csv 1.2345

where output.csv is the plain-text output filename and 1.2345 is the velocity Uo

Input file format: 
Col 0 : Time
Col 1 : y
Col 2 : U
Col 3 : V
Col 4 : T

(Time doesn't matter, quantities are integrated for each y value.)
"""
import sys

# cross-section of concern
fn = sys.argv[1]
Uo = float(sys.argv[2])

# diameter of the orifice, = 6/512
d0 = 0.01171875

try:
    f = open(fn)
    #~ f = open('out-3500-grp6-EXP10-Cs021.csv')
    #~ f = open('outpts-vicinity.csv')
except:
    print "Error opening file"
    raise 

count = {}
sumU = {}
sumV = {}
sumT = {}
sumsqrU = {}
sumsqrV = {}
sumsqrT = {}
sumUV = {}
sumUT = {}
sumVT = {}
avgU = {}
avgV = {}
avgT = {}
Urms = {}
Vrms = {}
Trms = {}
covUV = {}
covUT = {}
covVT = {}
intensityU = {}
intensityV = {}
intensityT = {}
intensityUV = {}
intensityUT = {}
intensityVT = {}
Umax = {}
Vmax = {}
Tmax = {}

# ok, these are predefine values in the xyz location files
# using these saves us to setup the dictionaries faster
# we could also use try ... except as well
# but repeating for each data line is too time-consuming
allXpos = [0.086, 0.203, 0.320, 0.438, 0.555, 0.672, 0.789, 0.906]

for xpos in allXpos:
    count[xpos] = {}
    sumU[xpos] = {}
    sumV[xpos] = {}
    sumT[xpos] = {}
    sumsqrU[xpos] = {}
    sumsqrV[xpos] = {}
    sumsqrT[xpos] = {}
    sumUV[xpos] = {}
    sumUT[xpos] = {}
    sumVT[xpos] = {}
    avgU[xpos] = {}
    avgV[xpos] = {}
    avgT[xpos] = {}
    Urms[xpos] = {}
    Vrms[xpos] = {}
    Trms[xpos] = {}
    covUV[xpos] = {}
    covUT[xpos] = {}
    covVT[xpos] = {}
    intensityU[xpos] = {}
    intensityV[xpos] = {}
    intensityT[xpos] = {}
    intensityUV[xpos] = {}
    intensityUT[xpos] = {}
    intensityVT[xpos] = {}

for line in f:
    rec = line.split()
    xpos = float(rec[0])
    # rounding for d because values on diagonal profiles appear to be approximate
    # here y is the radial coordinate, r
    ypos = round(float(rec[1])*200) / 200 
    try:
        count[xpos][ypos] += 1
    except:
        count[xpos][ypos] = 1
    U = float(rec[2])				# rec[6] if used original output file, rec[2] if used shortened file
    V = float(rec[3])				# rec[6] if used original output file, rec[2] if used shortened file
    T = float(rec[4])				# rec[6] if used original output file, rec[2] if used shortened file
    try:
        sumU[xpos][ypos] += U
        sumV[xpos][ypos] += V
        sumT[xpos][ypos] += T
    except:
        sumU[xpos][ypos] = U
        sumV[xpos][ypos] = V
        sumT[xpos][ypos] = T
    try:
        sumsqrU[xpos][ypos] += U * U
        sumsqrV[xpos][ypos] += V * V
        sumsqrT[xpos][ypos] += T * T
        sumUV[xpos][ypos] += U * V
        sumUT[xpos][ypos] += U * T
        sumVT[xpos][ypos] += V * T
    except:
        sumsqrU[xpos][ypos] = U*U
        sumsqrV[xpos][ypos] = V*V
        sumsqrT[xpos][ypos] = T*T
        sumUV[xpos][ypos] = U * V
        sumUT[xpos][ypos] = U * T
        sumUT[xpos][ypos] = U * T
        sumVT[xpos][ypos] = V * T


for xpos in allXpos:
    for ypos in count[xpos]:
        avgU[xpos][ypos] = sumU[xpos][ypos] / count[xpos][ypos]
        avgV[xpos][ypos] = sumV[xpos][ypos] / count[xpos][ypos]
        avgT[xpos][ypos] = sumT[xpos][ypos] / count[xpos][ypos]
        Urms[xpos][ypos] = ( sumsqrU[xpos][ypos] / count[xpos][ypos] - avgU[xpos][ypos]*avgU[xpos][ypos] ) ** 0.5
        Vrms[xpos][ypos] = ( sumsqrV[xpos][ypos] / count[xpos][ypos] - avgV[xpos][ypos]*avgV[xpos][ypos] ) ** 0.5
        Trms[xpos][ypos] = ( sumsqrT[xpos][ypos] / count[xpos][ypos] - avgT[xpos][ypos]*avgT[xpos][ypos] ) ** 0.5
        covUV[xpos][ypos] = sumUV[xpos][ypos] / count[xpos][ypos] - avgU[xpos][ypos]*avgV[xpos][ypos]
        covUT[xpos][ypos] = sumUT[xpos][ypos] / count[xpos][ypos] - avgU[xpos][ypos]*avgT[xpos][ypos]
        covVT[xpos][ypos] = sumVT[xpos][ypos] / count[xpos][ypos] - avgV[xpos][ypos]*avgT[xpos][ypos]

    Umax[xpos] = max(avgU[xpos].values())
    Vmax[xpos] = max(avgV[xpos].values())
    Tmax[xpos] = max(avgT[xpos].values())

    # normalize

    for ypos in count[xpos]:
        # avg[ypos] /= Umax  no need yet because we still want to compare decay in U ~ x
        intensityU[xpos][ypos] = Urms[xpos][ypos] / Umax[xpos] 	# to compare with Mi et al. (2001)
        intensityV[xpos][ypos] = Vrms[xpos][ypos] / Umax[xpos] 	# to compare with Mi et al. (2001)
        intensityUV[xpos][ypos] = covUV[xpos][ypos] / (Umax[xpos] * Umax[xpos]) 	# to compare with Mi et al. (2001)
        try:
            intensityT[xpos][ypos] = Trms[xpos][ypos] / Tmax[xpos] 	# to compare with Mi et al. (2001)
            intensityUT[xpos][ypos] = covUT[xpos][ypos] / (Umax[xpos] * Tmax[xpos]) 	# to compare with Mi et al. (2001)
            intensityVT[xpos][ypos] = covVT[xpos][ypos] / (Vmax[xpos] * Tmax[xpos]) 	# to compare with Mi et al. (2001)
        except:
            intensityT[xpos][ypos] = 0
            intensityUT[xpos][ypos] = 0
            intensityVT[xpos][ypos] = 0

print "Mean axial velocity of various cross-sections:"
print "        ", 
for xpos in allXpos:
    print "%7.3f" % ( (xpos+0.5)/d0 ) ,
print
for ypos in count[allXpos[0]]:      # count[anyXpos] would be okay
    print "%7.3f" % ( ypos / d0 ) ,
    for xpos in allXpos:
        print "%7.3f"  % (avgU[xpos][ypos] / Uo), 
    print 
