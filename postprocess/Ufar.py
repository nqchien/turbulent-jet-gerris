""" Reads a time series file from gerris
and plot velocity and turbulent intensity profile
of one cross section denoted by xcr.
This value of xcr is input as a parameter from the command line.
"""
import sys

# cross-section of concern
fn = sys.argv[1]
xcr = sys.argv[2]

try:
	f = open(fn)
	#~ f = open('out-3500-grp6-EXP10-Cs021.csv')
	#~ f = open('outpts-vicinity.csv')
except:
    print "Error opening file"
    raise 

count = {}
sum = {}
sumsqr = {}
avg = {}
stdev = {}
intensity = {}

for line in f:
    rec = line.split()
    if rec[1]==xcr:
        ypos = float(rec[2]) / 0.0625		#  0.03125 for r/r chart ; 0.0625 for r/d chart
        try:
            count[ypos] += 1
        except:
            count[ypos] = 1
        U = float(rec[3])				# rec[6] if used original output file, rec[3] if used shortened file
        try:
            sum[ypos] += U
        except:
            sum[ypos] = U
        try:
            sumsqr[ypos] += U * U
        except:
            sumsqr[ypos] = U*U

for ypos in count:
    avg[ypos] = sum[ypos] / count[ypos]
    stdev[ypos] = ((sumsqr[ypos] - count[ypos]*avg[ypos]*avg[ypos]) / (count[ypos] - 1) ) ** 0.5

Umax = max(avg.values())

# normalize
for ypos in count:
    # avg[ypos] /= Umax  no need yet because we still want to compare decay in U ~ x
    intensity[ypos] = stdev[ypos] / Umax 	# to compare with Mi et al. (2001)

print "Average:"
for k in count:
    print k, avg[k]
    
print "\nIntensity"
for k in count:
    print k, intensity[k]