#!/bin/env python

# Generate xyz file for points at various cross-sections downstream a turbulent jet
# at x/d = 6, 8, 10, 12, 14, 16
# jet centreline is on the plane xy, making an angle (ang) with x-axis
# jet origin is at (x=-0.5 , y=0 , z=0).

import math, sys

ang = float(sys.argv[1])	# input angle, in degrees
sin45 = cos45 = math.cos(math.radians(45))
cos_a = math.cos(math.radians(ang))
sin_a = math.sin(math.radians(ang))
tan_a = math.tan(math.radians(ang))

f = open('xs_PAP20_diag_near.pts', 'w')

d = 6.0 / 512
yo = -5 * d / 2
dl = 0.005  # output spacing on each axis 
num = 7  # number of points on each ray

xcls = [x_over_d * d * cos_a - 0.5 for x_over_d in range(6,17,2) ]
ycls = [yo + x_over_d * d * sin_a for x_over_d in range(6,17,2) ]

for (xcl,ycl) in zip(xcls, ycls):
    for y in range(-num, num+1):
        f.write(str(xcl-y*dl*sin_a) + " " + str(ycl+y*dl*cos_a) + " 0\n")   # 7*dl == 0.035 ~ 3d  reaching the boundary of jet at x = 15d
    for z in range(-num, num+1):
        f.write(str(xcl) +" "+ str(ycl) +" "+ str(z*dl) + "\n")
    for i in range(-num, num+1):
        f.write(str(xcl-i*dl*sin_a*cos45)+" "+str(ycl+i*dl*cos_a*cos45)+" "+str(i*dl*cos_a*sin45)+"\n") 
    for j in range(-num, num+1):
        f.write(str(xcl+j*dl*sin_a*cos45)+" "+str(ycl-j*dl*cos_a*cos45)+" "+str(j*dl*cos_a*sin45)+"\n")
f.close()
