""" Reads a time series file from gerris
and plot velocity and turbulent intensity profile
of one cross section denoted by xcr.
This value of xcr is input as a parameter from the command line.

Command line format:
python stats_profile.py output.csv 1.2345

where output.csv is the plain-text output filename and 1.2345 is the velocity Uo

Input file format: 
Col 0 : Time
Col 1 : y
Col 2 : U
Col 3 : V
Col 4 : T

(Time doesn't matter, quantities are integrated for each y value.)
"""
import sys

# cross-section of concern
fn = sys.argv[1]
Uo = float(sys.argv[2])

try:
    f = open(fn)
    #~ f = open('out-3500-grp6-EXP10-Cs021.csv')
    #~ f = open('outpts-vicinity.csv')
except:
    print "Error opening file"
    raise 

count = {}
sumU = {}
sumV = {}
sumT = {}
sumsqrU = {}
sumsqrV = {}
sumsqrT = {}
sumUV = {}
sumUT = {}
sumVT = {}
avgU = {}
avgV = {}
avgT = {}
Urms = {}
Vrms = {}
Trms = {}
covUV = {}
covUT = {}
covVT = {}
intensityU = {}
intensityV = {}
intensityT = {}
intensityUV = {}
intensityUT = {}
intensityVT = {}

for line in f:
    rec = line.split()
    ypos = float(rec[1]) / 0.01171875		#  = 6/512, diameter of the nozzle
    try:
        count[ypos] += 1
    except:
        count[ypos] = 1
    U = float(rec[2])				# rec[6] if used original output file, rec[2] if used shortened file
    V = float(rec[3])				# rec[6] if used original output file, rec[2] if used shortened file
    T = float(rec[4])				# rec[6] if used original output file, rec[2] if used shortened file
    try:
        sumU[ypos] += U
        sumV[ypos] += V
        sumT[ypos] += T
    except:
        sumU[ypos] = U
        sumV[ypos] = V
        sumT[ypos] = T
    try:
        sumsqrU[ypos] += U * U
        sumsqrV[ypos] += V * V
        sumsqrT[ypos] += T * T
        sumUV[ypos] += U * V
        sumUT[ypos] += U * T
        sumVT[ypos] += V * T
    except:
        sumsqrU[ypos] = U*U
        sumsqrV[ypos] = V*V
        sumsqrT[ypos] = T*T
        sumUV[ypos] = U * V
        sumUT[ypos] = U * T
        sumVT[ypos] = V * T

for ypos in count:
    avgU[ypos] = sumU[ypos] / count[ypos]
    avgV[ypos] = sumV[ypos] / count[ypos]
    avgT[ypos] = sumT[ypos] / count[ypos]
    Urms[ypos] = ( sumsqrU[ypos] / count[ypos] - avgU[ypos]*avgU[ypos] ) ** 0.5
    Vrms[ypos] = ( sumsqrV[ypos] / count[ypos] - avgV[ypos]*avgV[ypos] ) ** 0.5
    Trms[ypos] = ( sumsqrT[ypos] / count[ypos] - avgT[ypos]*avgT[ypos] ) ** 0.5
    covUV[ypos] = sumUV[ypos] / count[ypos] - avgU[ypos]*avgV[ypos]
    covUT[ypos] = sumUT[ypos] / count[ypos] - avgU[ypos]*avgT[ypos]
    covVT[ypos] = sumVT[ypos] / count[ypos] - avgV[ypos]*avgT[ypos]

Umax = max(avgU.values())
Tmax = max(avgT.values())

# normalize
for ypos in count:
    # avg[ypos] /= Umax  no need yet because we still want to compare decay in U ~ x
    intensityU[ypos] = Urms[ypos] / Umax 	# to compare with Mi et al. (2001)
    intensityV[ypos] = Vrms[ypos] / Umax 	# to compare with Mi et al. (2001)
    intensityUV[ypos] = covUV[ypos] / (Umax * Umax) 	# to compare with Mi et al. (2001)
    try:
        intensityT[ypos] = Trms[ypos] / Tmax 	# to compare with Mi et al. (2001)
        intensityUT[ypos] = covUT[ypos] / (Umax * Tmax) 	# to compare with Mi et al. (2001)
        intensityVT[ypos] = covVT[ypos] / (Vmax * Tmax) 	# to compare with Mi et al. (2001)
    except:
        intensityT[ypos] = intensityUT[ypos] = intensityVT[ypos] = 0
    
#~ print "y/d, U/Uo, V, T, Urms/Ucl, Vrms/Ucl, Trms/Ucl, UV/Ucl2, UT/Ucl2, VT/Ucl2:"
#~ for k in count:
    #~ print k, avgU[k] / Uo, avgV[k], avgT[k], intensityU[k], intensityV[k], intensityT[k], intensityUV[k],  intensityUT[k],  intensityVT[k]

print "y/d, U/Uo, y/d, Urms/Ucl, Vrms/Ucl, UV/Ucl2:"
for k in count:
    print "%7.3f;%7.3f;%7.3f;%7.3f;%7.3f;%7.3f"  % (k, avgU[k] / Uo, k, intensityU[k], intensityV[k], intensityUV[k])
